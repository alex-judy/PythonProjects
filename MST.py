'''Created by Alex Judy using
Python Language, matplotlib,
and networkx libraries'''


import matplotlib.pyplot as plt #library for displaying GUI
import networkx as nx #library for creating GUI
import math

d = {} #dictionary to store values frin text file
with open("Offices.txt") as f: #file I/O
    for line in f:
        (node, x, y) = line.split(',')
        d[node] = [int(x),int(y)]


G = nx.Graph() #Graph data struct that exists in networkx
for spot in d: #turns data in
    G.add_node(spot, x=d[spot][0], y=d[spot][1])
G.add_nodes_from(d)

print G.node #checking to make sure all data is transferred to G correctly

for node1 in G.nodes(): #this will create our edges using node1 and node2 as iterators
    for node2 in G.nodes():
        if node1 != node2:
            G.add_edge(node1, node2, weight = (math.sqrt(pow(G.node[node1]['x']- G.node[node2]['x'],2) + pow(G.node[node1]['y']- G.node[node2]['y'],2))))


G.remove_edge('C','D') #we can add more of these if we want to remove more than one edge. or comment out and keep all edges
G=nx.minimum_spanning_tree(G) #if we comment this out we will have a complete graph with all edges

nx.draw(G, pos = d) #this will "draw" our graph
nx.draw_networkx_edges(G, pos = d, edge_color = 'b') #we can edit attributes in our graph with these commands
nx.draw_networkx_labels(G, pos = d)
nx.draw_networkx_edge_labels(G, pos = d)

plt.show() #finally we utilize the pyplot library to display our graph